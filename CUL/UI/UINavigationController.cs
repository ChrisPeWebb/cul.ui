﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CUL.UI
{
	/// <summary>
	/// Handles the loading, caching, and creating of UIViews.
	/// <para>Contained a managed view stack, as well as the ability to create unmanaged persistent views.</para>
	/// </summary>
	public class UINavigationController : MonoBehaviour
	{
		// the path in which we will look for UIViews to load them into our view list
		[SerializeField]
		private string m_UIViewPath = "UIViews";

		[SerializeField]
		private string m_rootView = "";

		// should the UI be persistent between scenes
		[SerializeField]
		private bool m_persistentUI = true;

		// should the manager attempt to load all available views on awake?
		[SerializeField]
		private bool m_loadAllViewsOnAwake = true;

		// should the manager cache every views on awake?
		[SerializeField]
		private bool m_cacheAllViewsOnAwake = false;

		// when enabled, the state manager will draw gui to the screen showing which views are cached/active/registered
		[SerializeField]
		private bool m_enableDebug = true;

		// when enabled, all ui view functionality will log
		[SerializeField]
		private bool m_verboseLogs = false;

		private static UINavigationController m_instance;

		private static object m_lock = new object();

		private static bool m_applicationIsQuitting = false;

		private Dictionary<string,GameObject> m_loadedUIViews = new Dictionary<string, GameObject>();

		private Dictionary<string,UIView> m_cachedUIViews = new Dictionary<string, UIView>();

		private Dictionary<string,UIView> m_persistentUIViews = new Dictionary<string, UIView>();

		// There are a few reasons this isnt a stack. Dont worry, im not an idiot
		private List<string> m_viewStack = new List<string>();

		private UIView m_stackTopView = null;

		private Transform m_cachedTransform;

		// If the controller is busy when attempting to do a push/pop/add/remove, the command not complete, and will allow the original operation to continue.
		private bool m_isBusy = false;


		/// <summary>
		/// Gets the loaded UIViews.
		/// </summary>
		/// <value>The loaded UIViews.</value>
		public Dictionary<string,GameObject> LoadedUIViews
		{
			get { return m_loadedUIViews; }
		}


		/// <summary>
		/// Gets the cached UIViews.
		/// </summary>
		/// <value>The cached UIViews.</value>
		public Dictionary<string,UIView> CachedUIViews
		{
			get { return m_cachedUIViews; }
		}


		/// <summary>
		/// Gets the persistent UIViews.
		/// </summary>
		/// <value>The persistent UIViews.</value>
		public Dictionary<string,UIView> PersistentUIViews
		{
			get { return m_persistentUIViews; }
		}


		/// <summary>
		/// Gets the UIView stack.
		/// </summary>
		/// <value>The UIView stack.</value>
		public List<string> ViewStack
		{
			get { return m_viewStack; }
		}


		/// <summary>
		/// Gets the stack top UIView.
		/// </summary>
		/// <value>The stack top UIView.</value>
		public UIView StackTopView
		{
			get { return m_stackTopView; }
		}


		public static UINavigationController Instance
		{
			get
			{
				if( m_applicationIsQuitting )
					return null;

				lock( m_lock )
				{
					if( !m_instance )
					{
						m_instance = (UINavigationController)FindObjectOfType( typeof( UINavigationController ) );

						if( FindObjectsOfType( typeof( UINavigationController ) ).Length > 1 )
						{
							return m_instance;
						}

						if( !m_instance )
						{
							GameObject singleton = new GameObject();
							m_instance = singleton.AddComponent<UINavigationController>();
							singleton.name = "UINavigationController";
						} 
					}

					return m_instance;
				}
			}
		}


		public void OnDestroy()
		{
			m_applicationIsQuitting = true;
		}


		private void Awake()
		{
			m_cachedTransform = GetComponent<Transform>();

			if( m_persistentUI )
				DontDestroyOnLoad( gameObject );

			if( m_loadAllViewsOnAwake )
				LoadViewsFromResources();

			if( m_cacheAllViewsOnAwake )
				CacheAllViews();

			if( m_rootView != "" )
			{
				Push( m_rootView );
			}
		}


		/// <summary>
		/// Loads all views found at the Resource folder path specific by m_UIViewPath.
		/// <para>Views will then be ready to cache, push or addPersistent</para>
		/// </summary>
		public void LoadViewsFromResources()
		{
			// TODO Allow for proper lazy loading of views
            
			GameObject[] loadedUIViews = Resources.LoadAll<GameObject>( m_UIViewPath );

			if( m_verboseLogs )
				Debug.Log( loadedUIViews.Length + " UIViews loaded from resources." );

			if( loadedUIViews.Length == 0 )
				Debug.LogWarning( "Could not load any UIViews from the path: " + m_UIViewPath );

			for( int i = 0; i < loadedUIViews.Length; i++ )
			{
				m_loadedUIViews.Add( loadedUIViews[i].name, loadedUIViews[i] );
			}
		}


		/// <summary>
		/// All loaded views will be cached.
		/// </summary>
		public void CacheAllViews()
		{
			foreach( string viewName in m_loadedUIViews.Keys )
			{
				CachView( viewName );
			}
		}


		/// <summary>
		/// Caches a view so that it is ready to push or addPersistent.
		/// </summary>
		/// <returns>The UIView component attatched to the cached view.</returns>
		/// <param name="viewName">The name of the view to cache.</param>
		public UIView CachView( string viewName )
		{
			if( m_cachedUIViews.ContainsKey( viewName ) )
			{
				if( m_verboseLogs )
					Debug.LogWarning( "Attemping to cache view which has already been cached: [" + viewName + "]" );

				return null;
			}

			if( !m_loadedUIViews.ContainsKey( viewName ) )
			{
				Debug.LogError( "Attempting to cache view which has not been loaded: [" + viewName + "]" );
				return null;
			}
				
			GameObject tempGO = (GameObject)Instantiate( m_loadedUIViews[viewName] );
			tempGO.name = viewName;

			UIView tempView = tempGO.GetComponent<UIView>();

			if( !tempView )
			{
				Debug.LogError( "Could not find UIView component on view: [" + viewName + "]" );
				return null;
			}

			tempView.Initialize();
			tempView.SetViewGOActive( false );

			m_cachedUIViews.Add( tempGO.name, tempView );

			if( m_persistentUI )
				DontDestroyOnLoad( tempGO );

			Transform tempTransform = tempGO.GetComponent<Transform>();

			tempTransform.SetParent( m_cachedTransform, false );

			if( m_verboseLogs )
				Debug.Log( "Cached View: [" + viewName + "]" );

			return tempView;
		}


		/// <summary>
		/// UnCaches a view. The view will be removed from the scene if it is not already.
		/// <para>Attempting to Push or AddPersistent this view in the future will require re-caching (done automatically, or manually).</para>
		/// </summary>
		/// <param name="viewName">The name of the view to unCache.</param>
		public void UnCacheView( string viewName )
		{
			if( !m_cachedUIViews.ContainsKey( viewName ) )
			{
				Debug.LogWarning( "Attemping to uncache view which is not cached: [" + viewName + "]" );
				return;
			}

			if( m_cachedUIViews[viewName] == null )
			{
				m_cachedUIViews.Remove( viewName );
			}

			Destroy( m_cachedUIViews[viewName].gameObject );
			m_cachedUIViews.Remove( viewName );

			if( m_verboseLogs )
				Debug.Log( "View uncached viewName: [" + viewName + "]" );
		}


		/// <summary>
		/// Pushes a view to the the top of the managed view stack. The previous top view view will be deactivated.
		/// <para>If the previous view cannot be deactivated, or new view cannot be activated, no change will occure.</para>
		/// <para>The new view will be cached if required.</para>
		/// <para>If the new view already exists as a persistent view, no change will occure.</para>
		/// </summary>
		/// <returns>The UIView component attatched to the pushed view.</returns>
		/// <param name="viewName">The name of the view to push.</param>
		public UIView Push( string viewName, bool pushToFront = true )
		{
			if( m_isBusy )
			{
				Debug.LogWarning( "Attemping to push view while currently doing a view operation: [" + viewName + "]" );
				return null;
			}

			m_isBusy = true;

			// Check if the view is not already persistent or on the top of the stack
			if( m_stackTopView && m_stackTopView.ViewName == viewName )
			{
				Debug.LogWarning( "Attemping to push view which is currently at the top of the view stack. Returning existing view: [" + viewName + "]" );
				m_isBusy = false;
				return m_stackTopView;
			}

			if( m_persistentUIViews.ContainsKey( viewName ) )
			{
				Debug.LogWarning( "Attemping to push view which is already active: [" + viewName + "]" );
				m_isBusy = false;
				return null;
			}

			// Deactivate current
			if( m_stackTopView )
			{
				m_stackTopView.ChangeState( UIViewState.INACTIVE );
			}

			// Ensure new is cached or can be cached
			if( !m_cachedUIViews.ContainsKey( viewName ) )
			{
				if( m_verboseLogs )
					Debug.LogWarning( "Attemping to push view which has not been cached. Will attempt to cache: [" + viewName + "]" );

				CachView( viewName );

				if( !m_cachedUIViews.ContainsKey( viewName ) )
				{
					Debug.LogError( "Could not push view. View could not be cached: [" + viewName + "]" );
					m_isBusy = false;
					return null;
				}
			}

			if( m_cachedUIViews[viewName] == null )
			{
				Debug.LogWarning( "Attemping to push view whose cache is null. Will attempt to recache: [" + viewName + "]" );
				m_cachedUIViews.Remove( viewName );
				CachView( viewName );

				if( !m_cachedUIViews.ContainsKey( viewName ) )
				{
					Debug.LogError( "Could not push view. View could not be cached: [" + viewName + "]" );
					m_isBusy = false;
					return null;
				}
			}

			// Activate the view
			m_stackTopView = m_cachedUIViews[viewName];
			m_stackTopView.SetViewGOActive( true );

			m_stackTopView.ChangeState( UIViewState.ACTIVE );

			if( m_verboseLogs )
				Debug.Log( "Pushed view: [" + viewName + "]" );

			// Add the view to the stack
			m_viewStack.Add( viewName );

			m_isBusy = false;

			if( pushToFront )
			{
				m_stackTopView.transform.SetAsLastSibling();
			}

			return m_stackTopView;
		}


		/// <summary>
		/// The managed stack will pop the the first element. The previous top view will be deactivated.
		/// <para>If the previous view cannot be deactivated, or new view cannot be activated, no change will occure.</para>
		/// <para>The new view will be cached if required.</para>
		/// <para>If there there is only one element in the managed stack, no change will occure.</para>
		/// <para>If the new view already exists as a persistent view, no change will occure.</para>
		/// </summary>
		public void PopToRoot()
		{
			if( m_isBusy )
			{
				Debug.LogWarning( "Attemping to pop to root while currently doing a view operation" );
				return;
			}

			m_isBusy = true;

			// check if we can create previous view
			if( m_viewStack.Count > 1 )
			{
				string newStackTopViewName = m_viewStack[0];

				// Check if the new top view is not already persistent or on the top of the stack
				if( m_stackTopView && m_stackTopView.ViewName == newStackTopViewName )
				{
					Debug.LogWarning( "Attemping to pop to view which is currently at the top of the view stack. [" + newStackTopViewName + "]" );
					m_isBusy = false;
					return;
				}

				if( m_persistentUIViews.ContainsKey( newStackTopViewName ) )
				{
					Debug.LogWarning( "Attemping to pop to view which is already active: [" + newStackTopViewName + "]" );
					m_isBusy = false;
					return;
				}

				// Before we get rid of the current top, ensure that we can actually cache the previous view if it is not already
				if( !m_cachedUIViews.ContainsKey( newStackTopViewName ) )
				{
					Debug.LogWarning( "Attemping to pop to view which has not been cached. Will attempt to cache: [" + newStackTopViewName + "]" );
					CachView( newStackTopViewName );

					if( !m_cachedUIViews.ContainsKey( newStackTopViewName ) )
					{
						Debug.LogError( "Could not pop to root. View could not be cached: [" + newStackTopViewName + "]" );
						m_isBusy = false;
						return;
					}
				}

				if( m_cachedUIViews[newStackTopViewName] == null )
				{
					Debug.LogWarning( "Attemping to pop to view whose cache is null. Will attempt to recache: [" + newStackTopViewName + "]" );
					m_cachedUIViews.Remove( newStackTopViewName );
					CachView( newStackTopViewName );

					if( !m_cachedUIViews.ContainsKey( newStackTopViewName ) )
					{
						Debug.LogError( "Could not pop to root. View could not be cached: [" + newStackTopViewName + "]" );
						m_isBusy = false;
						return;
					}
				}

				// Deactivate current
				if( m_stackTopView )
				{
					m_stackTopView.ChangeState( UIViewState.INACTIVE );
				}

				// Clear stack except for first element
				m_viewStack.RemoveRange( 1, m_viewStack.Count - 1 );

				// Activate previous (now the top of the stack)
				m_stackTopView = m_cachedUIViews[newStackTopViewName];
				m_stackTopView.SetViewGOActive( true );

				m_stackTopView.ChangeState( UIViewState.ACTIVE );

				if( m_verboseLogs )
					Debug.Log( "Poped to view: [" + newStackTopViewName + "]" );

			}
			else if( m_viewStack.Count == 1 )
			{
				Debug.LogWarning( "Attempting to pop to root, but we are alreaedy at root: [" + m_stackTopView.ViewName + "]" );
			}
			else
			{
				Debug.LogWarning( "Attempting to pop empty stack." );
			}

			m_isBusy = false;
		}


		/// <summary>
		/// Removes the top element from the managed view stack and activates the next view.
		/// <para>The new view will be cached if required.</para>
		/// <para>If the previous view cannot be deactivated, or new view cannot be activated, no change will occure.</para>
		/// <para>If the new view already exists as a persistent view, no change will occure.</para>
		/// </summary>
		public void Pop()
		{
			if( m_isBusy )
			{
				Debug.LogWarning( "Attemping to pop view while currently doing a view operation" );
				return;
			}

			m_isBusy = true;

			// check if we can create previous view
			if( m_viewStack.Count > 1 )
			{
				string newStackTopViewName = m_viewStack[m_viewStack.Count - 2];

				// Check if the new top view is not already persistent or on the top of the stack
				if( m_stackTopView && m_stackTopView.ViewName == newStackTopViewName )
				{
					Debug.LogWarning( "Attemping to pop to view which is currently at the top of the view stack. [" + newStackTopViewName + "]" );
					m_isBusy = false;
					return;
				}

				if( m_persistentUIViews.ContainsKey( newStackTopViewName ) )
				{
					Debug.LogWarning( "Attemping to pop to view which is already active: [" + newStackTopViewName + "]" );
					m_isBusy = false;
					return;
				}

				// Before we get rid of the current top, ensure that we can actually cache the previous view if it is not already
				if( !m_cachedUIViews.ContainsKey( newStackTopViewName ) )
				{
					Debug.LogWarning( "Attemping to pop to view which has not been cached. Will attempt to cache: [" + newStackTopViewName + "]" );
					CachView( newStackTopViewName );

					if( !m_cachedUIViews.ContainsKey( newStackTopViewName ) )
					{
						Debug.LogError( "Could not pop to view. View could not be cached: [" + newStackTopViewName + "]" );
						m_isBusy = false;
						return;
					}
				}

				if( m_cachedUIViews[newStackTopViewName] == null )
				{
					Debug.LogWarning( "Attemping to pop to view whose cache is null. Will attempt to recache: [" + newStackTopViewName + "]" );
					m_cachedUIViews.Remove( newStackTopViewName );
					CachView( newStackTopViewName );

					if( !m_cachedUIViews.ContainsKey( newStackTopViewName ) )
					{
						Debug.LogError( "Could not pop to view. View could not be cached: [" + newStackTopViewName + "]" );
						m_isBusy = false;
						return;
					}
				}

				// Deactivate current
				if( m_stackTopView )
				{
					m_stackTopView.ChangeState( UIViewState.INACTIVE );
				}

				// Pop current 
				m_viewStack.RemoveAt( m_viewStack.Count - 1 ); 

				// Activate previous (now the top of the stack)
				m_stackTopView = m_cachedUIViews[newStackTopViewName];
				m_stackTopView.SetViewGOActive( true );

				m_stackTopView.ChangeState( UIViewState.ACTIVE );

				if( m_verboseLogs )
					Debug.Log( "Poped to view: [" + newStackTopViewName + "]" );

			}
			else if( m_viewStack.Count == 1 )
			{
				m_stackTopView.ChangeState( UIViewState.INACTIVE );

				m_viewStack.RemoveAt( m_viewStack.Count - 1 ); 
				m_stackTopView = null;

				if( m_verboseLogs )
					Debug.LogWarning( "Stack is now empty." );
			}
			else
			{
				Debug.LogWarning( "Attempting to pop empty stack." );
			}
			m_isBusy = false;
		}


		/// <summary>
		/// All previous stack histroy will be forgotten. The current stack top will become the root.
		/// </summary>
		public void ClearStackHistory()
		{
			if( m_isBusy )
			{
				Debug.LogWarning( "Attemping to clear history while currently doing a view operation" );
				return;
			}

			m_viewStack.RemoveRange( 0, m_viewStack.Count - 1 );
		}


		/// <summary>
		/// An un-managed view will be created. The view will not be effected by any future push or pop operations.
		/// <para>The view will be cached if required.</para>
		/// <para>If the view already exists as persistent, no change will occure.</para>
		/// <para>If the view already exists on the managed view stack, no change will occure.</para>
		/// </summary>
		/// <returns>The UIView component attatched to the created view.</returns>
		/// <param name="viewName">The name of the view to add persistent.</param>
		public UIView AddPersistent( string viewName, bool pushToFront = true )
		{
			if( m_isBusy )
			{
				Debug.LogWarning( "Attemping to add persistent view while currently doing a view operation: [" + viewName + "]" );
				return null;
			}

			m_isBusy = true;

			if( m_viewStack.Contains( viewName ) )
			{
				Debug.LogWarning( "Attemping to add view which is currently on the view stack: [" + viewName + "]" );
				m_isBusy = false;
				return null;
			}
				
			if( m_persistentUIViews.ContainsKey( viewName ) )
			{
				Debug.LogWarning( "Attemping to add view which is already active. Returning existing view: [" + viewName + "]" );
				m_isBusy = false;
				return m_persistentUIViews[viewName];
			}

			if( !m_cachedUIViews.ContainsKey( viewName ) )
			{
				if( m_verboseLogs )
					Debug.LogWarning( "Attemping to add view which has not been cached. Will attempt to cache: [" + viewName + "]" );

				CachView( viewName );

				if( !m_cachedUIViews.ContainsKey( viewName ) )
				{
					Debug.LogError( "Could not add view. View could not be cached: [" + viewName + "]" );
					m_isBusy = false;
					return null;
				}
			}

			if( m_cachedUIViews[viewName] == null )
			{
				Debug.LogWarning( "Attemping to add view whose cache is null. Will attempt to recache: [" + viewName + "]" );
				m_cachedUIViews.Remove( viewName );
				CachView( viewName );

				if( !m_cachedUIViews.ContainsKey( viewName ) )
				{
					Debug.LogError( "Could not add view. View could not be cached: [" + viewName + "]" );
					m_isBusy = false;
					return null;
				}
			}

			UIView tempView = m_cachedUIViews[viewName];
			tempView.SetViewGOActive( true );

			tempView.ChangeState( UIViewState.ACTIVE );

			m_persistentUIViews.Add( viewName, tempView );

			if( m_verboseLogs )
				Debug.Log( "Activated State: [" + viewName + "]" );

			m_isBusy = false;

			if( pushToFront )
			{
				tempView.transform.SetAsLastSibling();
			}

			return tempView;
		}


		/// <summary>
		/// Removes a persistent view.
		/// </summary>
		/// <param name="viewName">The name of the persistent view to be removed.</param>
		public void RemovePersistent( string viewName )
		{
			if( m_isBusy )
			{
				Debug.LogWarning( "Attemping to remove persistent view while currently doing a view operation: [" + viewName + "]" );
				return;
			}

			m_isBusy = true;

			if( m_verboseLogs )
				Debug.Log( "Attempting to remove persistent view: [" + viewName + "]" );

			if( !m_persistentUIViews.ContainsKey( viewName ) )
			{
				Debug.LogError( "Could not remove persistent view. View not currently active: [" + viewName + "]" );
				m_isBusy = false;
				return;
			}

			m_persistentUIViews[viewName].ChangeState( UIViewState.INACTIVE );

			// It might be a good idea to give views a callback, so that they can be destroyed from the scene once they have finished deactivating themselves.
			// As it stands currently, views are responsible for destroying themselves
			// Finish perhaps? Also add uncache after bool to UIView

			m_persistentUIViews.Remove( viewName );

			if( m_verboseLogs )
				Debug.Log( "Removed persistent view: [" + viewName + "]" );

			m_isBusy = false;
		}


		/// <summary>
		/// Renders the debug UI, displaying the views that are loaded, cached, persistent and the view stack.
		/// </summary>
		private void OnGUI()
		{
			if( m_enableDebug )
			{
				GUILayout.BeginVertical( "box" );
				if( m_viewStack != null )
				{
					if( m_viewStack.Count > 0 )
					{
						GUILayout.Box( "View Stack" );
					}

					for( int i = 0; i < m_viewStack.Count; i++ )
					{
						GUILayout.Label( m_viewStack[i] );
					}
				}

				GUILayout.Space( 10.0f );
				if( m_persistentUIViews != null )
				{
					if( m_persistentUIViews.Keys.Count > 0 )
					{
						GUILayout.Box( "Persistent Views" );
					}

					foreach( string key in m_persistentUIViews.Keys )
					{
						GUILayout.Label( key );
					}
				}

				GUILayout.Space( 10.0f );
				if( m_cachedUIViews != null )
				{
					if( m_cachedUIViews.Count > 0 )
					{
						GUILayout.Box( "Cached Views" );
					}

					foreach( string key in m_cachedUIViews.Keys )
					{
						GUILayout.Label( key );
					}
				}

				GUILayout.EndVertical();
			}
		}
	}
}
