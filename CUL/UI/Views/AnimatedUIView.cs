﻿using UnityEngine;
using System.Collections;

namespace CUL.UI
{
	/// <summary>
	///	A simple animated view. 
	/// <para>The selected animation clip will be played forwards when the view is activated, and backwards when it is deactivated. </para>
	/// <para>Supports uncaching after deactivation.</para>
	/// </summary>
	[RequireComponent( typeof( Animation ) )]
	public class AnimatedUIView : UIView
	{
		[SerializeField]
		private bool m_unCacheOnDeactivate = false;


		public bool UnCacheOnDeactivate
		{
			get{ return m_unCacheOnDeactivate; }
			set{ m_unCacheOnDeactivate = value; }
		}


		[SerializeField]
		private AnimationClip m_animationClip;

		private Animation m_animation = null;


		protected override void ActivateView()
		{
			if( !m_animation )
			{
				m_animation = gameObject.GetComponent<Animation>();
				m_animation.playAutomatically = false;
				m_animation.wrapMode = WrapMode.Once;
				m_animation.AddClip( m_animationClip, "anim_inout" );
			}

			StopAllCoroutines();
				
			m_animation.Stop();
			m_animation["anim_inout"].speed = 1.0f;
			m_animation["anim_inout"].time = 0.0f;
			m_animation.Play( "anim_inout", PlayMode.StopAll );
		}


		protected override void DeactivateView()
		{
			StopAllCoroutines();

			m_animation.Stop();
			m_animation["anim_inout"].speed = -1.0f;
			m_animation["anim_inout"].time = m_animation["anim_inout"].length;
			m_animation.Play( "anim_inout", PlayMode.StopAll );

			StartCoroutine( AttemptUnCacheAfterTime( m_animation["anim_inout"].length ) );
		}


		private IEnumerator AttemptUnCacheAfterTime( float time )
		{
			yield return new WaitForSeconds( time );

			if( m_unCacheOnDeactivate )
			{
				UINavigationController.Instance.UnCacheView( ViewName );
			}
			else
			{
				SetViewGOActive( false );
			}
		}
	}
}