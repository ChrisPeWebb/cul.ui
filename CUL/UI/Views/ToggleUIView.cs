﻿using UnityEngine;
using System.Collections;

namespace CUL.UI
{
	/// <summary>
	/// A simple view in which the gameobject is simple toggled on or off.
	/// <para>Supports uncaching after deactivation.</para>
	/// </summary>
	public class ToggleUIView : UIView
	{
		[SerializeField]
		private bool m_unCacheOnDeactivate = false;


		public bool UnCacheOnDeactivate
		{
			get{ return m_unCacheOnDeactivate; }
			set{ m_unCacheOnDeactivate = value; }
		}


		protected override void ActivateView()
		{
		}


		protected override void DeactivateView()
		{
			if( m_unCacheOnDeactivate )
			{
				UINavigationController.Instance.UnCacheView( ViewName );
			}
			else
			{
				SetViewGOActive( false );
			}
		}
	}
}
