﻿using UnityEngine;
using System.Collections;

namespace CUL.UI
{
	/// <summary>
	/// A simple animated view. 
	/// <para>The selected animation will be played in when the view is activated, and out when the view is deactivated.</para>
	/// <para>Supports uncaching after deactivation.</para>
	/// </summary>
	[RequireComponent( typeof( Animation ) )]
	public class DualAnimatedUIView : UIView
	{
		[SerializeField]
		private bool m_unCacheOnDeactivate = false;


		public bool UnCacheOnDeactivate
		{
			get{ return m_unCacheOnDeactivate; }
			set{ m_unCacheOnDeactivate = value; }
		}


		[SerializeField]
		private AnimationClip m_animationClipIn;

		[SerializeField]
		private AnimationClip m_animationClipOut;

		private Animation m_animation = null;


		protected override void ActivateView()
		{
			if( !m_animation )
			{
				m_animation = gameObject.GetComponent<Animation>();
				m_animation.playAutomatically = false;
				m_animation.wrapMode = WrapMode.Once;
				m_animation.AddClip( m_animationClipIn, "anim_in" );
				m_animation.AddClip( m_animationClipOut, "anim_in" );
			}
	
			m_animation["anim_in"].time = 0.0f;
			m_animation.Play( "anim_in", PlayMode.StopAll );

			StopAllCoroutines();
		}


		protected override void DeactivateView()
		{
			StopAllCoroutines();

			m_animation["anim_out"].time = 0.0f;
			m_animation.Play( "anim_out", PlayMode.StopAll );

			StartCoroutine( AttemptUnCacheAfterTime( m_animation["anim_out"].length ) );
		}


		private IEnumerator AttemptUnCacheAfterTime( float time )
		{
			yield return new WaitForSeconds( time );

			if( m_unCacheOnDeactivate )
			{
				UINavigationController.Instance.UnCacheView( ViewName );
			}
			else
			{
				SetViewGOActive( false );
			}
		}
	}
}