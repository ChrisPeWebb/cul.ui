﻿using UnityEngine;
using System.Collections;

namespace CUL.UI
{
	/// <summary>
	/// Allows a UIView to be controlled/extended via components. Each ViewController on the same game object as a UIView will recieve lifecycle notifications.
	/// </summary>
	public abstract class UIViewController : MonoBehaviour
	{
		public bool IsActive
		{
			get { return ( View.State == UIViewState.ACTIVE ); }
		}


		/// <summary>
		/// Gets or sets a value indicating whether this view controller is busy.
		/// </summary>
		/// <value><c>true</c> if this view controller is busy; otherwise, <c>false</c>.</value>
		public bool IsBusy
		{
			get; 
			set;
		}


		/// <summary>
		/// The UIView that this controller belongs to.
		/// </summary>
		public UIView View
		{
			get; 
			set;
		}


		/// <summary>
		/// The view is about to activate. 
		/// </summary>
		public virtual void ViewWillActivate()
		{
		}


		/// <summary>
		/// The view was just activated.
		/// </summary>
		public virtual void ViewDidActivate()
		{
		}


		/// <summary>
		/// The view is about to deactivate. 
		/// </summary>
		public virtual void ViewWillDeactivate()
		{
		}


		/// <summary>
		/// The view was just deactivated.
		/// </summary>
		public virtual void ViewDidDeactivate()
		{
		}
	}
}
