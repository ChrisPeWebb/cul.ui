﻿using UnityEngine;
using System.Collections;

namespace CUL.UI
{
	public enum UIViewState
	{
		INACTIVE,
		ACTIVE}

	;

	/// <summary>
	/// Represents a single user interface screen.
	/// </summary>
	public abstract class UIView : MonoBehaviour
	{
		private string m_viewName;


		/// <summary>
		/// The name of the view, extracted from its GameObjects name on instantiation/cache.
		/// </summary>
		public string ViewName
		{
			get{ return m_viewName; }
		}


		private UIViewState m_state = UIViewState.INACTIVE;


		/// <summary>
		/// The current state of the view. If the view is transitioning, returns UIViewState.INACTIVE
		/// </summary>
		public UIViewState State
		{
			get{ return m_state; }
		}


		/// <summary>
		/// Helper property. The current state of the view. If the view is transitioning, returns false
		/// </summary>
		public bool IsActive
		{
			get{ return ( m_state == UIViewState.ACTIVE ); }
		}


		private UIViewController[] m_viewControllers;


		/// <summary>
		/// The view controllers currently attatched to this view.
		/// </summary>
		public UIViewController[] ViewControllers
		{ 
			get { return m_viewControllers; } 
		}


		/// <summary>
		/// Sets up the view so that it is ready to be cached or activated. The view will get its name from its game objects name.
		/// <para>All UIViewControllers on the same game object will be found and added to the life cycle invocation list.</para>
		/// </summary>
		public void Initialize()
		{
			m_viewName = this.gameObject.name;

			m_viewControllers = GetComponents<UIViewController>();

			for( int i = 0; i < m_viewControllers.Length; i++ )
			{
				m_viewControllers[i].View = this;
			}
		}


		/// <summary>
		/// Requests the view to change state.
		/// </summary>
		/// <returns><c>true</c>, if state was changed successfully, <c>false</c> otherwise.</returns>
		/// <param name="newState">New state.</param>
		public void ChangeState( UIViewState newState )
		{
			if( newState == UIViewState.ACTIVE && m_state != UIViewState.ACTIVE )
			{
				// pre setting the state prevents view from activating/deactivating itself from within the activate/deactivate methods, which will cause issues
				m_state = UIViewState.ACTIVE;

				// Let each viewController know that it will acivate.
				for( int i = 0; i < m_viewControllers.Length; ++i )
				{
					m_viewControllers[i].ViewWillActivate();
				}

				ActivateView();

				// Let each view controller know it has been activated
				for( int i = 0; i < m_viewControllers.Length; ++i )
				{
					m_viewControllers[i].ViewDidActivate();
				}

			}
			else if( newState == UIViewState.INACTIVE && m_state != UIViewState.INACTIVE )
			{
				m_state = UIViewState.INACTIVE;

				// Let each viewController know that it will deacivate.
				for( int i = 0; i < m_viewControllers.Length; ++i )
				{
					m_viewControllers[i].ViewWillDeactivate();
				}

				DeactivateView();

				// Let each view controller know it has been deactivated
				for( int i = 0; i < m_viewControllers.Length; ++i )
				{
					m_viewControllers[i].ViewDidDeactivate();
				}
			}
		}


		/// <summary>
		/// Changes the views game object active state
		/// </summary>
		/// <param name="active">If set to <c>true</c> gameobject set to active.</param>
		public void SetViewGOActive( bool active )
		{
			gameObject.SetActive( active );
		}


		/// <summary>
		/// Helper method to find an attatched UIViewController. Basically the same as a get component, really.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns>The first UIViewController found of type T attatched to this view. Otherwise returns null.</returns>
		public T GetViewController<T>() where T : UIViewController
		{
			for( int i = 0; i < m_viewControllers.Length; i++ )
			{
				if( m_viewControllers[i] is T )
				{
					return (T)m_viewControllers[i];
				}
			}

			return null;
		}


		/// <summary>
		/// Adds a new persistent view to the UINavigationController.
		/// <para>Will only succeed if this view is currently active.</para>
		/// </summary>
		/// <returns>The UIView component attatched to the created view. If this view is not active, will return null.</returns>
		/// <param name="viewName">The name of the view to add persistent.</param>
		public UIView AddPersistent( string viewName )
		{
			return ( IsActive ) ? UINavigationController.Instance.AddPersistent( viewName ) : null;
		}


		/// <summary>
		/// Helper class for the editor (as methods with a return value will not be shown in NGUI or UGUI delegate list).
		/// <para>Adds a new persistent view to the UINavigationController.</para>
		/// <para>Will only succeed if this view is currently active.</para>
		/// </summary>
		/// <param name="viewName">View name.</param>
		public void AddPersistentEditor( string viewName )
		{
			if( IsActive )
				UINavigationController.Instance.AddPersistent( viewName );
		}


		/// <summary>
		/// Removes a persistent view.
		/// <para>Will only succeed if this view is currently active.</para>
		/// </summary>
		/// <param name="viewName">The name of the persistent view to be removed.</param>
		public void RemovePersistent( string viewName )
		{
			if( IsActive )
				UINavigationController.Instance.RemovePersistent( viewName );
		}


		/// <summary>
		/// Removes this view if it is persistent.
		/// <para>Will only succeed if this view is currently active.</para>
		/// </summary>
		public void RemovePersistentSelf()
		{
			if( IsActive )
				UINavigationController.Instance.RemovePersistent( ViewName );
		}


		/// <summary>
		/// Removes the top element from the managed view stack and activates the next view.
		/// <para>The new view will be cached if required.</para>
		/// <para>If the previous view cannot be deactivated, or new view cannot be activated, no change will occur.</para>
		/// <para>If the new view already exists as a persistent view, no change will occur.</para>
		/// <para>Will only succeed if this view is currently active.</para>
		/// </summary>
		public UIView Push( string viewName )
		{
			return ( IsActive ) ? UINavigationController.Instance.Push( viewName ) : null;
		}


		/// <summary>
		/// Helper class for the editor (as methods with a return value will not be shown in NGUI or UGUI delegate list).
		/// <para>Removes the top element from the managed view stack and activates the next view.</para>
		/// <para>The new view will be cached if required.</para>
		/// <para>If the previous view cannot be deactivated, or new view cannot be activated, no change will occur.</para>
		/// <para>If the new view already exists as a persistent view, no change will occur.</para>
		/// <para>Will only succeed if this view is currently active.</para>
		/// </summary>
		public void PushEditor( string viewName )
		{
			if( IsActive )
				UINavigationController.Instance.Push( viewName );
		}


		/// <summary>
		/// Removes the top element from the managed view stack and activates the next view.
		/// <para>The new view will be cached if required.</para>
		/// <para>If the previous view cannot be deactivated, or new view cannot be activated, no change will occure.</para>
		/// <para>If the new view already exists as a persistent view, no change will occure.</para>
		/// <para>Will only succeed if this view is currently active.</para>
		/// </summary>
		public void Pop()
		{
			if( IsActive )
				UINavigationController.Instance.Pop();
		}


		/// <summary>
		/// The managed stack will pop the the first element. The previous top view will be deactivated.
		/// <para>If the previous view cannot be deactivated, or new view cannot be activated, no change will occure.</para>
		/// <para>The new view will be cached if required.</para>
		/// <para>If there there is only one element in the managed stack, no change will occure.</para>
		/// <para>If the new view already exists as a persistent view, no change will occure.</para>
		/// <para>Will only succeed if this view is currently active.</para>
		/// </summary>
		public void PopToRoot()
		{
			if( IsActive )
				UINavigationController.Instance.PopToRoot();
		}


		/// <summary>
		/// All previous stack histroy will be forgotten. The current stack top will become the root.
		/// </summary>
		public void ClearStackHistory()
		{
			UINavigationController.Instance.ClearStackHistory();
		}


		/// <summary>
		/// Caches a view so that it is ready to push or addPersistent.
		/// <para>Will only succeed if this view is currently active.</para>
		/// </summary>
		/// <returns>The UIView component attatched to the cached view.</returns>
		/// <param name="viewName">The name of the view to cache.</param>
		public UIView CacheView( string viewName )
		{
			return ( IsActive ) ? UINavigationController.Instance.CachView( viewName ) : null;
		}


		/// <summary>
		/// UnCaches a view. The view will be removed from the scene if it is not already.
		/// <para>Attempting to Push or AddPersistent this view in the future will require re-caching (done automatically, or manually).</para>		
		/// <para>Will only succeed if this view is currently active.</para>
		/// </summary>
		/// <param name="viewName">The name of the view to unCache.</param>
		public void UnCacheView( string viewName )
		{
			if( IsActive )
				UINavigationController.Instance.UnCacheView( viewName );
		}


		/// <summary>
		/// Activates the view.
		/// </summary>
		protected virtual void ActivateView()
		{
		}


		/// <summary>
		/// Deactivates the view.
		/// </summary>
		protected virtual void DeactivateView()
		{
		}
	}
}