##Overview / Features##
CUL.UI helps to simplify window management. 

Views are saved as prefabs in the resources folder, and are loaded by the **UINavigationController** at launch.

Views can be precached (added to the scene, deactivated) to reduce stutter when transitioning into complex views.

Views can be automatically cached, and there are options to keep the views cached after they are deactivated.

**UINavigationController** contains a managed stack, in which views can be pushed and popped. View history can be deleted, or navigation can be pushed to the root. This is useful for menus.

**UINavigationController** contains an unmanaged (persistent) list of views, which are not affected by push/pop operations. This is useful for in game UI, which the player shouldn't be able to remove via the hardware back button, or any UI elements which can exists at the same time.

**UIView** can be inherited to allow for different activate/deactivate functions. By default, there are UIViews that support simple toggling, and single/dual animations. (as well as a simple nGUI panel fade view)

**UIViewController** can be inherited and added to the same game object as a **UIView** to further extend functionality (in a modular fashion). **UIViewController** receive lifecycle updates.

**UIViewController** can be used for "driver" scripts, which communicate with nGUI/uGUI, and by using the in-built IsActive property, input can be limited to when the state is active only, and not transitioning.

##Documentation##
Auto-generated documentation is available here:
https://s3-us-west-2.amazonaws.com/vivec/CUL.UI_Docs/html/annotated.html

##Planned Features##
[USE ISSUE TRACKER]

Please let me know if there is any functionality you want.

##Known Issues##

[USE ISSUE TRACKER]

Please let me know if you find any issues.

##Creating a User Interface##

###UGUI###
[Ill add a tutorial to this I swear]

###NGUI###
[This is probably out of date. Has not been updated for a while]
**[Simple Set up]**

- Create a new scene
- Create a folder in Resources named "UIViews"
- Add a UIRoot to the scene
- Add a UINavigationController to the UIRoot
- Create a new UIPanel, with the UIRoot as the parent
- Add a ToggleUIView to the panel
- Rename the panel "Main" (or anything! this is the name of the view, which we can use to reference it)
- Do whatever you want to the panel. Anchor it, add a button, background, whatever
- Create a new prefab in the UIViews resource folder
- Drag the panel we just created into the prefab
- Delete the panel from the scene
- On the UINavigationController, set the root view to be "Main", or whatever you called the view we just created
- Save and run!

**[Communicating Views]**

- Using the same approach previously, create a new view, named "FirstView", and use the NGUIFadeUIView instead of a ToggleUIView
- Add a button to the view, with the label "submit"
- Add a text input box to the view
- Set the UINavigationController root view to "FirstView"
- Create a new script called "FirstViewController"
- Using CUL.UI
- Inherit from UIViewController
- Add a public UIInput variable
- Add a public function "SubmitButtonOnClick"
- Save the script, and add it to the same gameobject that the NGUIFadeView is on
- Connect the UIInput reference to the input text box on the view
- Set the submit buttons on click method to be the "SubmitButtonOnClick" on the controller script
- Save the prefab, then delete it from the scene
- Ill add the rest I swear


###Immediate###